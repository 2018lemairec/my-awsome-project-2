import CoordTOdist
import travel_time_stiff
import mobility_histogramm


def indice(ville_depart, ville_arrivee):
    " Takes in arguments two cities in type str\
    and returns the mobility index of the start_city in relation to the arrival_city in type float"
    
    data = mobility_histogramm.load_data_mobility("mobilites-professionnelles-en-2014.csv",mobility_histogramm.list_department_code)
    
    for i in range(len(data)):
        if data[i]["start_city_name"] == ville_depart and data[i]["arrival_city_name"] == ville_arrivee:
            long_depart_mauvais_format = data[i]["start_coordinates"][1]
            long_depart = long_depart_mauvais_format[1:9]
            lat_depart_mauvais_format = data[i]["start_coordinates"][0]
            lat_depart = lat_depart_mauvais_format[:9]
            long_arrivee_mauvais_format = data[i]["arrival_coordinates"][1]
            long_arrivee = long_arrivee_mauvais_format[1:9]
            lat_arrivee_mauvais_format = data[i]["arrival_coordinates"][0]
            lat_arrivee = lat_arrivee_mauvais_format[:9]
        
    temps_reel = travel_time_stiff.travel_time(long_depart, lat_depart, long_arrivee, lat_arrivee)
    
    vitesse_moy = 70 / 3.6      # en m/s
    
    distance_voloiseau = CoordTOdist.distance_voloiseau(float(lat_depart_mauvais_format), float(long_depart_mauvais_format), float(lat_arrivee_mauvais_format), float(long_arrivee_mauvais_format))
    
    temps_voloiseau = distance_voloiseau / vitesse_moy
    
    indice = temps_voloiseau/temps_reel
    return indice,temps_voloiseau
    


if __name__ == '__main__':
    print(indice("Villejuif","Massy"))