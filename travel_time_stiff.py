import csv
import os
import sys


path_mobility=os.path.abspath(os.path.join(os.path.basename(sys.argv[0]),"..", "accessibilite-des-arrets-de-bus-ratp.csv"))

# Liste des régions étudiées qu'on viendra trier dans la base de données
list_department_code=["75","91","92","77","93","95","94","78"]



def load_data_mobility(file_name):

    '''On organise toutes nos bases de données sous
    la forme de liste de dictionnaires'''

    #Voici notre liste de disctionnaires
    L=[]

    try:
        with open(file_name) as csvfile:

            # On ouvre le fichier
            file = csv.reader(csvfile, delimiter=';', quotechar=' ')
            next(file)

            #On parcourt les éléments d'une ligne pour les ajouter dans un dictionnaire
            for row in file:
                a={'code_insee':str(row[2]), 'access_ufr':str(row[6]), 'annonce_sonore':str(row[7]), 'annonce_visuelle':str(row[8]), 'annonce_son_perturb':float(row[9]),'annonce_vis_perturb':row[10].split(',')}

                #Si la ville de départ ET la ville d'arrivée appartiennent à la liste des départements étudiés, on ajoute le dictionnaire
                #if a['start_region_code'][:2] in list_department_code and a['arrival_region_code'][:2] in list_department_code:
                 #   L.append(a)
        return(L)
    except OSError:
        raise NameError('file not found')

print(load_data_mobility(path_mobility))
