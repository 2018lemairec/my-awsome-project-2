import csv
import os
import sys
import mobility_histogramm

path_access=os.path.abspath(os.path.join(os.path.basename(sys.argv[0]),"..", "accessibilite-des-arrets-de-bus-ratp.csv"))
path_ville=os.path.abspath(os.path.join(os.path.basename(sys.argv[0]),"..", "villes_france.csv"))
path_mobility=os.path.abspath(os.path.join(os.path.basename(sys.argv[0]),"..", "mobilites-professionnelles-en-2014.csv"))

# Liste des régions étudiées qu'on viendra trier dans la base de données
list_department_code=["75","91","92","77","93","95","94","78"]

#Gère les exceptions lors d'oubli d'information dans la base de données
def exception(res):
    if res=='':
        return '0'
    else:
        return res

#Cette fonction retourne le nombre d'arrêt, la note totale des arrêts et le nombre d'accès handicapé pour une ville
def info_arret_bus(code_commune):

    '''On organise toutes nos bases de données sous
    la forme de liste de dictionnaires'''

    #Voici notre liste de disctionnaires
    nombre_arret=0
    note_totale=0
    note_access_ufr=0
    try:
        with open(path_access) as csvfile:

            # On ouvre le fichier
            file = csv.reader(csvfile, delimiter=';', quotechar=' ')


            #On parcourt les éléments d'une ligne pour les ajouter dans un dictionnaire
            for row in file:

                #a={'code_insee':int(row[2]), 'region_code':int(row[3]), 'access_ufr':int(exception(str(row[6]))), 'annonce_sonore':int(str(row[7])), 'annonce_visuelle':int(str(row[8])), 'annonce_son_perturb':int(str(row[9])),'annonce_vis_perturb':int(str(row[10]))}

                #Si la ville de départ ET la ville d'arrivée appartiennent à la liste des départements étudiés, on ajoute le dictionnaire
                if str(row[2])==code_commune:
                    nombre_arret+=1
                    access_ufr=int(exception(str(row[6])))
                    annonce_sonore=int(str(row[7]))
                    annonce_visuelle=int(str(row[8]))
                    annonce_son_perturb=int(str(row[9]))
                    annonce_vis_perturb=int(str(row[10]))
                    note_un_arret=annonce_sonore+annonce_visuelle+annonce_son_perturb+annonce_vis_perturb
                    note_access_ufr+=access_ufr
                    note_totale+=note_un_arret
        return(note_totale,nombre_arret,note_access_ufr)
    except OSError:
        raise NameError('file not found')

#Cette focntion renvoie la densité de population d'une ville
def info_ville(code_commune):
    try:
        with open(path_ville) as csvfile1:

            # On ouvre le fichier
            file = csv.reader(csvfile1, delimiter=',', quotechar=' ')
            next(file)

            #On parcourt les éléments d'une ligne pour les ajouter dans un dictionnaire
            for row in file:
                #Si la ville de départ ET la ville d'arrivée appartiennent à la liste des départements étudiés, on ajoute le dictionnaire
                if code_commune in row[10][1:-1]:
                    return(int(row[17][1:-1]))



        return(True)
    except OSError:
        raise NameError('file not found')


def rapport_nbrearret_densite(code_commune):
    return info_arret_bus(code_commune)[1]/info_ville(code_commune)

#Ce code permet de déterminer le rapport nombre d'arrêt de bus/densité maximale en île de France
#Cela permet de réaliser un échantillonnage des données pour pouvoir donner une note relativement à ce rapport
#A noter que les arrondissements de Paris sont hors-catégories car ils possèdent un rapport trop important pour réaliser une échelle linéaire
"""
Ce code est relativement long à tourner ... La valuer maximale retenue est 0.05 obentue par la ville de Wissous
def max_bus_dens():
    L=mobility_histogramm.load_data_mobility(path_mobility,list_department_code)
     #L=[{'start_region_code':'94080','start_city_name':'Vincennes'}]
    max=0
    ville_deja_analyse=[]
    for dic in L:
        if int(dic['start_region_code']) not in ville_deja_analyse :
            ville_deja_analyse.append(int(dic['start_region_code']))
            rapport=float(rapport_nbrearret_densite(dic['start_region_code']))
            if rapport>=max:
                print(rapport,dic['start_region_code'],dic['start_city_name'])
                max=rapport
    return max
"""
def pourcentage(code_commune):
    rapport=rapport_nbrearret_densite(code_commune)
    bus=info_arret_bus(code_commune)
    if rapport>=0.05:
        pt_rap=70
    else :
        pt_rap=70*rapport/0.05
    pt_bus=bus[0]/bus[1]/4*30
    return pt_bus+pt_rap

def access_handic(code_commune):
    handic=info_arret_bus(code_commune)[2]
    tot=info_arret_bus(code_commune)[0]
    return handic/tot*100

print(pourcentage('75115'))
