import requests

api_key = "AIzaSyBbYOSDmLIzzIpKesvX520PW74nBxZz7xo"

def distance_temps_voiture(lat_depart, long_depart, lat_arrivee, long_arrivee):
    " Takes in argument str : the GPS coordinate of 2 points A & B\
    and returns the distance between the points in meters and the time of travel using a car in seconds"
    
    argument = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + lat_depart +","+ long_depart + "&destinations=" + lat_arrivee + "," + long_arrivee +'&key=' +api_key
    request = requests.get(argument)
    json = request.json()
    return json["rows"][0]["elements"][0]["distance"]["value"],json["rows"][0]["elements"][0]["duration"]["value"]

if __name__ == '__main__' :
    print(distance_temps_voiture("48.730756","2.27137","48.792716","2.359279"))