import csv
import os
import sys
import pprint
import folium
import pandas as pd
from IPython.display import HTML, display
from folium.plugins import MarkerCluster
import codecs

# Permet à tout le monde d'avoir le chemin qui donne accés au fichier
path_mobility=os.path.abspath(os.path.join(os.path.basename(sys.argv[0]),"..", "mobilites-professionnelles-en-2014-deplacements-domicile-lieu-de-travail.csv"))

# Liste des régions étudiées qu'on viendra trier dans la base de données
list_department_code=["75","91","92","77","93","95","94","78"]



def load_data_mobility(file_name,list_department_code):

    '''On organise toutes nos bases de données sous
    la forme de liste de dictionnaires'''

    #Voici notre liste de disctionnaires
    L=[]

    try:
        with codecs.open(file_name, "rb", "utf-8") as csvfile:

            # On ouvre le fichier
            file = csv.reader(csvfile, delimiter=';', quotechar=' ')
            next(file)

            #On parcourt les éléments d'une ligne pour les ajouter dans un dictionnaire
            for row in file:
                a={'start_region_code':str(row[0]), 'start_city_name':str(row[1]), 'arrival_region_code':str(row[2]), 'arrival_city_name':str(row[3]), 'unknown':float(row[4]),'start_coordinates':row[5].split(','),'arrival_coordinates':row[6].split(',') }

                #Si la ville de départ ET la ville d'arrivée appartiennent à la liste des départements étudiés, on ajoute le dictionnaire
                if a['start_region_code'][:2] in list_department_code and a['arrival_region_code'][:2] in list_department_code:
                    L.append(a)
        return(L)
    except OSError:
        raise NameError('file not found')


def cluster_map(arrival_city):
    """
    :param arrival_city: str of arrival_city in lower case
    :return: interctive map showing the starting cities of people who travel to work at arrival city
    """
    coordinates = [48.7277896426, 2.27513131903] #Coordinates of Massy
    base_map = folium.Map(location=coordinates, tiles="OpenStreetMap", zoom_start=12) #Creating a basemap centralised at Massy
    data = load_data_mobility(str(path_mobility),list_department_code)
    #pprint.pprint(data)
    marker_cluster = MarkerCluster().add_to(base_map)
    for i in data:
        if i["arrival_city_name"].lower() == arrival_city:
            i['start_coordinates'] = [float(a) for a in i['start_coordinates']]
            folium.Marker(i['start_coordinates'], popup = i['start_city_name']).add_to(marker_cluster)
    return base_map.save("map.html")
print(cluster_map('massy'))
