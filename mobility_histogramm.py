import csv
import os
import sys
import matplotlib.pyplot as plt
import codecs

# Permet à tout le monde d'avoir le chemin qui donne accès au fichier
path_mobility=os.path.abspath(os.path.join(os.path.basename(sys.argv[0]),"..", "mobilites-professionnelles-en-2014.csv"))

# Liste des régions étudiées qu'on viendra trier dans la base de données
list_department_code=["75","91","92","77","93","95","94","78"]



def load_data_mobility(file_name,list_department_code):

    '''On organise toutes nos bases de données sous
    la forme de liste de dictionnaires'''

    #Voici notre liste de disctionnaires
    L=[]

    try:
        with codecs.open(file_name, "rb", "utf-8") as csvfile:

            # On ouvre le fichier
            file = csv.reader(csvfile, delimiter=';', quotechar=' ')
            next(file)

            #On parcourt les éléments d'une ligne pour les ajouter dans un dictionnaire
            for row in file:
                a={'start_region_code':str(row[0]), 'start_city_name':str(row[1]), 'arrival_region_code':str(row[2]), 'arrival_city_name':str(row[3]), 'unknown':float(row[4]),'start_coordinates':row[5].split(','),'arrival_coordinates':row[6].split(',') }

                #Si la ville de départ ET la ville d'arrivée appartiennent à la liste des départements étudiés, on ajoute le dictionnaire
                if a['start_region_code'][:2] in list_department_code and a['arrival_region_code'][:2] in list_department_code:
                    L.append(a)
        return(L)
    except OSError:
        raise NameError('file not found')


#print(load_data_mobility(str(path_mobility),list_department_code))

def input_arrival_city(city_name,data):

    '''Fonction qui prend le nom d'une vile et renvoie le dictionnaire
    des villes de départ en clé et pour valeur le nombre de départs
    de cette ville
    La variable data est deja sous forme de liste de dictionnaires'''

    #'''Liste contenant toutes les villes de départs
    #itérées plusieurs fois'''
    flux={}
    for arrival_city in data:
        if  arrival_city['arrival_city_name']== city_name:
            flux[arrival_city['start_city_name']]=int(arrival_city['unknown']*10**3)
            #Donne le nombre de gens qui viennent dans la ville d'arrivée en fonction de chaque ville de départ
    return(flux)

#print(len(input_arrival_city('Vincennes',load_data_mobility(path_mobility,list_department_code))))

def sort_select_max(dic_non_sorted):
    # trie sur les valeurs du dictionnaire
    dic_sorted=sorted(dic_non_sorted.items(),key=lambda t: t[1])
    dic_selec=dic_sorted[-10:0:-1]
    dic_selec=dic_selec[0:10]
    dic_selec.reverse()
    list_cities=[]
    list_people=[]
    for s in dic_selec:
        list_cities.append(s[0])
        list_people.append(s[1])
    return([list_cities,list_people])

#print(sort_select_max(input_arrival_city('Vincennes',load_data_mobility(path_mobility,list_department_code))))

def histo_(list_cities,list_people,city_name):
    plt.barh(list_cities, list_people, align='center', alpha=0.5)
    plt.ylabel('Nombre de personnes')
    plt.title('Provenance des travailleur de {}'.format(city_name))

    plt.show()

histo_(sort_select_max(input_arrival_city('Vincennes',load_data_mobility(path_mobility,list_department_code)))[0],sort_select_max(input_arrival_city('Vincennes',load_data_mobility(path_mobility,list_department_code)))[1],'Vincennes')








